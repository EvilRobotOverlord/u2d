const path = require('path');
const Typedoc = require('typedoc-webpack-plugin');
const yargs = require('yargs');
const DtsBundle = require('dts-bundle-webpack')

let plugins;

if (yargs.argv.plugins === 'true') {
  plugins = [
    new Typedoc({
      out: './build/doc',
      mode: 'file',
      excludePrivate: true,
      target: 'commonjs'
    }),

    new DtsBundle({
      name: 'u2d',
      main: './build/dec/exports.d.ts',
      out: '../u2d.d.ts',
      removeSource: true,
      indent: '  '
    })
  ];
} else {
  plugins = [];
}

module.exports = {
  entry: './src/exports.ts',
  mode: 'development',
  watch: yargs.argv.plugins !== 'true',

  output: {
    filename: './build/u2d.min.js',
    path: path.resolve(__dirname),
    library: 'U2D',
  },

  resolve: {
    extensions: ['.ts'],
  },

  module: {
    rules: [{
      test: /\.ts$/,
      use: 'ts-loader',
    }],
  },

  plugins
};
