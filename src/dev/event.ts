/**
 * Manages events so that they can be added and fired easily
 * 
 * @summary Event controller
 */
export default class Event<T extends Function> {
  handlers: T[] = [];

  constructor() { }

  /**
   * Add a new event listener to the specified event
   * @param handler Event handler
   */
  public addListener(handler: T): void {
    this.handlers.push(handler);
  }

  /**
   * Remove a handler from this event
   * @param handler Handler to remove
   */
  public removeListener(handler: T): void {
    this.handlers.splice(this.handlers.indexOf(handler), 1);
  }

  /**
   * Fires all event handlers attached to a certain event
   * @param e Event name
   * @param thisValue `this` value to pass to handlers
   * @param args Arguments to be passed to the handler
   */
  public fire(...args: any[]): this {
    this.handlers.forEach(el => el(...args));
    return this;
  }
}
