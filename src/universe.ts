import BufferExecutor from './util/buffer-executor';
import Event from "./dev/event";
import GameObject from './gameobject/gameobject';
import { Vector } from './util/vector';

// to be executed when the document becomes ready
let readyBuffer = new BufferExecutor();

if (document.readyState === 'interactive') {
  readyBuffer.execute();
} else {
  document.addEventListener('DOMContentLoaded', () => readyBuffer.execute());
}

/**
 * Class for interacting with a HTML5 `CanvasRenderingContext2D` (main class you will need)
 */
export default class Universe {
  private objects: GameObject[] = [];
  private dim: Vector;
  private canvas: HTMLCanvasElement;
  private lastFrame: number; // timestamp of drawing previous frame
  private running: boolean;
  private animId: number; // ID of requestAnimationFrame

  /**
   * Context onto which this universe renders
   */
  public ctx: CanvasRenderingContext2D;

  /**
   * Set drawing framerate (does not affect how often things are drawn, only
   * affects the rate at which values are updated)
   */
  public fps: number = 60;

  /**
   * Event fired when each frame has finished drawing
   */
  public drawEvent = new Event<(u: Universe, ctx: CanvasRenderingContext2D) => void>();

  /**
   * Event fired when the canvas is added to the document
   */
  public canvasEvent = new Event<(canvas: HTMLCanvasElement) => void>();

  /**
   * Initializes a new 2D `Universe`
   * 
   * @param width Width of the canvas
   * @param height Height of the canvas
   * @param appendTo HTML element to append canvas to
   */
  constructor(width: number, height: number, appendTo: string = 'body') {
    this.dim = new Vector(width, height);
    readyBuffer.queue(() => this.addCanvas(appendTo));
  }

  private addCanvas(appendTo: string) {
    this.canvas = document.createElement('canvas');
    [this.canvas.width, this.canvas.height] = this.dim.destruct();
    document.querySelector(appendTo).appendChild(this.canvas);

    this.ctx = this.canvas.getContext('2d');
    this.lastFrame = performance.now();
    this.running = true;

    this.canvasEvent.fire(this.canvas);
    this.animId = requestAnimationFrame(() => this.draw());
  }

  /**
   * Stops drawing and requesting more animation frames
   * 
   * @summary Pauses the game
   */
  pause() {
    cancelAnimationFrame(this.animId);
    this.running = false;
  }

  /**
   * Restarts drawing and requesting animation frames
   * 
   * @summary Resumes the game
   */
  resume() {
    this.lastFrame = performance.now();
    this.animId = requestAnimationFrame(() => this.draw());
    this.running = true;
  }

  /**
   * Adds a `GameObject` to the list of objects in the `Universe` so that it is drawn onto the canvas the next time the screen is updated
   * 
   * @summary Adds an object to the game
   * @param obj Object to be added
   */
  add(obj: GameObject) {
    this.objects.push(obj);
  }

  /**
   * Remove a `GameObject` from the array of objects in the `Universe`
   * @param obj 
   */
  remove(obj: GameObject) {
    let i;
    if ((i = this.objects.indexOf(obj)) > -1) {
      this.objects.splice(i, 1);
    }
  }

  /**
   * Move an objects Z position to just in front of another's
   * 
   * @summary Change Z in front of another object
   * @param obj Object to change the Z of
   * @param before Object to put `obj` in front of
   */
  moveBehind(obj: GameObject, before: GameObject) {
    let objIndex, beforeIndex;

    if ((objIndex = this.objects.indexOf(obj)) === -1 ||
      (beforeIndex = this.objects.indexOf(before)) === -1) {
      throw new Error(
        'Error: object could not be found in array'
      );
    }

    this.objects.splice(objIndex, 1);
    this.objects.splice(beforeIndex, 0, obj);
  }

  /**
   * Draws all objects onto the canvas
   */
  private draw() {
    if (!this.running) {
      return;
    }

    this.animId = requestAnimationFrame(this.draw.bind(this));
    const currTime = performance.now();
    const delta = (currTime - this.lastFrame) * this.fps / 1000;

    this.ctx.fillStyle = 'black';
    this.ctx.fillRect(0, 0, this.dim.x, this.dim.y);

    for (const obj of this.objects) {
      try {
        obj.move(this.dim, delta);
        obj.draw(this.ctx, currTime);
      } catch (err) {
        console.log(err);
      }
    }

    this.lastFrame = currTime;
    this.drawEvent.fire(this, this.ctx);
  }
}
