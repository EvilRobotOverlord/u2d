/**
 * Executes a buffer with an optional interval
 * 
 * @summary Executes functions in a buffer
 */
export default class BufferExecutor {
  /**
   * List of functions that will be executed when {@link BufferExecutor#execute|this `BufferExecutor` is executed}.
   * 
   * @summary List of queued functions
   */
  public actionList: Function[] = [];

  /**
   * Delay between the executions of each function in the `BufferExecutor`
   * 
   * @summary Delay between functions
   */
  public delay: number = 0;

  /**
   * Boolean indicating whether the buffer has finished executing
   * 
   * @summary Finished executing
   */
  public executed: boolean = false;

  /**
   * Boolean indicating whether the buffer is allowed to continue executing
   */
  private continueExec: boolean = true;

  /**
   * Initializes an empty buffer
   */
  constructor(delay?: number) {
    this.delay = delay || 0;
  }

  /**
   * Recursively execute buffer
   */
  private async recurseExec(callback: (e?: Error) => any) {
    if (this.actionList.length === 0 || !this.continueExec) {
      callback();
      return;
    }

    try {
      await this.actionList.shift()();
    } catch (e) {
      callback(e);
    }

    setTimeout(() => this.recurseExec(callback), this.delay);
  }

  /**
   * Adds a `function` to the queue. The `function` is executed immediately if the queue has already finished executing.
   * 
   * @summary Queue a `function`
   * @param fn Function to queue
   * @returns Chainable `this` value
   */
  queue(fn: (...args: any[]) => any): this {
    if (!this.executed) {
      this.actionList.push(fn);
    } else {
      fn();
    }

    return this;
  }

  /**
   * Pause the execution of the `BufferExecutor`. Note that if a function was previously executing, that function will execute completely before it stops.
   * 
   * @summary Pause execution
   */
  pause() {
    this.continueExec = false;
  }

  /**
   * Resume the execution of the buffer (if paused)
   * 
   * @summary Resume execution
   */
  resume() {
    if (!this.continueExec) {
      this.execute();
    }
  }

  /**
   * Execute all queued functions asynchronously. Removes the first element from {@link BufferExecutor#actionList|the `actionList`} each time it executes. Stops execution when the list is empty or if {@link BufferExecutor#pause|pause} is called.
   * 
   * @summary Begin execution
   * @param delay Delay between executions (ms)
   */
  execute(delay: number = 0) {
    /**
     * Boolean indicating whether execution is paused or is allowed to continue
     * 
     * @summary Continue execution
     * @member BufferExecutor#continueExec
     */
    this.continueExec = true;
    this.delay = delay;

    this.recurseExec(() => {
      this.executed = true;
    });
  }
}
