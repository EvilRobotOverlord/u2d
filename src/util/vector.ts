/**
 * Holds 2-dimensional vectors
 * 
 * # Usage
 ```typescript
 let vec = new Vector(10, 10);
 vec.add(new Vector(5, -5));
 
 console.log(vec); // [15, 5]
 ```
 * 
 * @summary 2D Vector
 */
export class Vector {
  public x: number;
  public y: number;

  /**
   * Creates a new `Vector`
   * 
   * @param x X value of the `Vector`
   * @param y Y value of the `Vector`
   */
  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  /**
   * Adds another `Vector` to `this`, mutating `this`
   * 
   * @summary Mutable add
   * @param n `Vector` to add to `this`
   * @returns New value of `this`
   */
  add(n: Vector): this {
    this.x += n.x;
    this.y += n.y;
    return this;
  }

  /**
   * Adds 2 `Vector`s, returning a new `Vector` without mutating any of the original `Vector`s
   * 
   * @summary Immutable add
   * @param a First `Vector`
   * @param b Second `Vector`
   * @returns `a + b`
   */
  static add(a: Vector, b: Vector): Vector {
    return new Vector(a.x + b.x, a.y + b.y);
  }

  /**
   * Flips the direction of and mutates `this`
   * 
   * @summary Mutable negative
   * @returns New value of `this`
   */
  neg(): this {
    this.x = -this.x;
    this.y = -this.y;
    return this;
  }

  /**
   * Returns a new `Vector` containing the negative of a `Vector` without mutating it
   * 
   * @summary Immutable negative
   * @param a `Vector` to get the negative of
   * @returns `-a`
   */
  static neg(a: Vector): Vector {
    return new Vector(-a.x, -a.y);
  }

  /**
   * Subtracts another `Vector` from and mutates `this`
   * 
   * @summary Mutable subract
   * @param n `Vector` to subtract
   * @returns New value of `this`
   */
  subtract(n: Vector): this {
    this.x -= n.x;
    this.y -= n.y;
    return this;
  }

  /**
   * Subtracts a `Vector` from another, returning a new `Vector` without mutating the original `Vector`s
   * 
   * @summary Immutable subtract
   * @param a `Vector` to be subtracted from
   * @param b `Vector` to be subtracted
   * @returns `a - b`
   */
  static subtract(a: Vector, b: Vector): Vector {
    return new Vector(a.x - b.x, a.y - b.y);
  }

  /**
   * Scales (multiplies) and mutate `this` by a number
   * 
   * @summary Mutable scale
   * @param n Number to scale by
   * @returns New value of `this`
   */
  scale(n: number): this {
    this.x *= n;
    this.y *= n;
    return this;
  }

  /**
   * Scales (multiplies) a `Vector` by a number without mutating it
   * 
   * @summary Immutable scale
   * @param vec `Vector` to scale
   * @param num Amount to scale by
   * @returns `vec * num`
   */
  static scale(vec: Vector, num: number): Vector {
    return new Vector(vec.x * num, vec.y * num);
  }

  /**
   * Clones `this`
   * 
   * @summary Copy `this`
   * @returns Clone of `this`
   */
  clone(): Vector {
    return new Vector(this.x, this.y);
  }

  /**
   * Clones a `Vector`
   * 
   * @summary Copy a `Vector`
   * @param vec Vector to clone
   * @returns Clone of `vec`
   */
  static clone(vec: Vector): Vector {
    return new Vector(vec.x, vec.y);
  }

  /**
   * Checks if a `Vector` is equal to `this`
   * 
   * @summary Instance equality test
   * @param vec `Vector` to compare `this` to
   * @returns `this` === vec`
   */
  equals(vec: Vector): boolean {
    return this.x === vec.x && this.y === vec.y;
  }

  /**
   * Checks if 2 `Vector`s are equal
   * 
   * @summary Static equality check
   * @param a LHS
   * @param b RHS
   * @returns `LHS === RHS`
   */
  static equals(a: Vector, b: Vector): boolean {
    return a.x === b.x && a.y === b.y;
  }

  /**
   * Returns strigified version of `this` in a matrix-like format
   * 
   * @summary Stringify `this`
   * @returns Stringified version of `this`
   */
  toString(): string {
    return `[${this.x}, ${this.y}]`;
  }

  /**
   * Returns `this` as an array of 2 values
   * 
   * @summary Convert `this` to array
   * @returns `[this.x, this.y]`
   */
  destruct(): [number, number] {
    return [this.x, this.y];
  }
}

/**
 * Shorthand for creating a new vector
 * @param x 
 * @param y 
 */
export function vec(x: number, y: number): Vector {
  return new Vector(x, y);
}
