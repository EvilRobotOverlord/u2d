import Flags from '../dev/flags';
import Event from '../dev/event';
import { Vector, vec } from '../util/vector';

/**
 * Base class for all objects in the game. Inherit this class to define new types of objects to be drawn onto the canvas. Do not use directly.
 * 
 * @summary Base class for game objects
 */
export default abstract class GameObject {
  protected pos: Vector;
  protected flags: number = 0;
  protected creation: number = performance.now();
  protected v: Vector = vec(0, 0);
  protected m: number = 100;
  protected force: Vector = vec(0, 0);

  /**
   * Opacity of the current shape that is used while drawing
   */
  public alpha: number = 1;

  /**
   * Event fired when the object hits an edge
   */
  public hitEdgeEvent = new Event<(obj: GameObject, axis: string) => void>();

  /**
   * Fired when the current object is drawn onto the canvas
   */
  public drawEvent = new Event<(self: GameObject, ctx: CanvasRenderingContext2D) => void>();

  /**
   * Draw `self` to the canvas. Called automatically each frame. Extensions are expected to call `drawEvent`
   * 
   * @summary (Internal) Draw `self`
   * @param ctx Context to draw onto
   * @param time Time elapsed since the creation of self (in ms)
   */
  abstract draw(_ctx: CanvasRenderingContext2D, _time: number);

  /**
   * Returns whether the object lies entirely to the right of some X co-ordinate
   * 
   * @summary Lies right of a vertical axis
   * @param x X co-ordinate to test
   */
  abstract liesRightOf(x: number): boolean;

  /**
   * Returns whether the object lies entirely to the left of some X co-ordinate
   * 
   * @summary Lies left of a vertical axis
   * @param x X co-ordinate to test
   */
  abstract liesLeftOf(x: number): boolean;

  /**
   * Returns whether the object lies entirely above some Y co-ordinate
   * 
   * @summary Lies above a horizontal axis
   * @param y Y co-ordinate to test
   */
  abstract liesAbove(y: number): boolean;

  /**
   * Returns whether the object lies entirely below some Y co-ordinate
   * 
   * @summary Lies below a virtual axis
   * @param y Y co-ordinate to test
   */
  abstract liesBelow(y: number): boolean;

  /**
   * Create a new `GameObject` at the specified position
   * 
   * @param pos Vector describing the position of the object
   */
  constructor(pos: Vector) {
    this.pos = pos;
  }

  /**
   * Get or set the X co-ordinate
   * 
   * @summary Get/set X
   * @param to Value to set the X co-ordinate to
   * @returns Current X co-ordinate
   */
  public x(to?: number): number {
    if (typeof to !== 'number') {
      return this.getCoords(performance.now() - this.creation).x;
    } else {
      this.pos.x = to;
      return to;
    }
  }

  /**
   * Get or set the Y co-ordinate
   * 
   * @summary Get/set Y
   * @param to Value to set the Y co-ordinate to
   * @returns Current Y co-ordinate
   */
  public y(to?: number): number {
    if (typeof to !== 'number') {
      return this.getCoords(performance.now() - this.creation).y;
    } else {
      this.pos.y = to;
      return to;
    }
  }

  /**
   * Get position vector at current frame. Designed to be overriden.
   * 
   * @summary (Overridable) Get co-ordinates
   * @param _t Time elapsed since the creation of self (in ms)
   * @returns Current position
   */
  public getCoords(_t: number): Vector {
    return this.pos;
  }

  /**
   * Get velocity vector at current frame. Designed to be overriden.
   * 
   * @summary (Overridable) Get velocity
   * @param t Time elapsed since the creation of self (in ms)
   * @returns Current velocity
   */
  public getVelocity(_t: number): Vector {
    return this.v;
  }

  /**
   * Get force acting on object at current frame. Designed to be overriden.
   * 
   * @summary (Overridable) Get force
   * @param _t Time elapsed since the creation of self (in ms)
   * @returns Vector representing the force acting on self
   */
  public getForce(_t: number): Vector {
    return this.force;
  }

  /**
   * Get or set a flag whether the gameobject can escape the canvas and leave its bounds
   * 
   * @summary Get or set whether the object can escape the canvas
   * @param bool Can escape the bounds of the canvas
   */
  public canEscapeCanvas(bool?: boolean): boolean {
    if (typeof bool === 'boolean') {
      if (bool) {
        this.flags |= Flags.canEscapeCanvas;
      } else {
        this.flags &= ~Flags.canEscapeCanvas;
      }

      return bool;
    }

    return Boolean(this.flags & Flags.canEscapeCanvas);
  }

  /**
   * Hide the object by setting the `hidden` flag to 0
   * 
   * @summary Hide self
   */
  public hide(): this {
    this.flags &= ~Flags.hidden;
    return this;
  }

  /**
   * Show the object by setting the `hidden` flag to 1
   * 
   * @summary Show self
   */
  public show(): this {
    this.flags |= Flags.hidden;
    return this;
  }

  /**
   * Returns whether the object is currently hidden (not drawn onto the canvas)
   * 
   * @summary Return hidden state
   * @returns Whether the current shape is hidden or not
   */
  public hidden(): boolean {
    return Boolean(this.flags & Flags.hidden);
  }

  /**
   * Get or set velocity vector
   * 
   * @summary Get/set velocity
   * @param v Vector to set velocity to
   * @returns Current velociy
   */
  public velocity(v?: Vector): Vector {
    if (v) {
      this.v = v;
      return v;
    } else {
      return this.getVelocity(performance.now() - this.creation);
    }
  }

  /**
   * Get or set the mass
   * 
   * @summary Get/set mass
   * @param m Mass to set to
   * @returns Current mass
   */
  public mass(m?: number): number {
    if (typeof m === "number") {
      this.m = m;
      return m;
    } else {
      return this.m;
    }
  }

  /**
   * Push the object, adding the specified force to the current force for `time` milliseconds
   * 
   * @summary Apply (add) force for some time
   * @param force Force to add
   * @param time Amount of time to push (in ms)
   */
  public pushXY(force: Vector, time: number = -1) {
    this.force.add(force);

    if (time > 0) {
      setTimeout(() => {
        this.force.subtract(force);
      }, time);
    }
  }

  /**
   * Internal function used to move object. Called automatically each frame.
   * 
   * @summary (Internal) Move `self`
   * @param {Vector} bounds Width and height of bounding rectangle
   * @param {number} delta Factor to multiply all changes by
   */
  public move(bounds: Vector, delta: number) {
    let t = performance.now() - this.creation;

    this.v.add(
      Vector.scale(this.getForce(t), delta / this.m)
    );

    let velocity = this.getVelocity(t);

    if (!(this.flags & Flags.canEscapeCanvas)) {
      if (
        (
          this.liesLeftOf(0) &&
          Math.sign(velocity.x) === -1
        ) || (
          this.liesRightOf(bounds.x) &&
          Math.sign(velocity.x) === 1
        )
      ) {
        this.hitEdgeEvent.fire(this, 'x');
        velocity = vec(0, velocity.y);
      }

      if (
        (
          this.liesAbove(0) &&
          Math.sign(velocity.y) === -1
        ) || (
          this.liesBelow(bounds.y) &&
          Math.sign(velocity.y) === 1
        )
      ) {
        this.hitEdgeEvent.fire(this, 'y');
        velocity = vec(velocity.x, 0);
      }
    }

    this.pos.add(velocity.scale(delta));
  }
}
