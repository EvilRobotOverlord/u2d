import GameObject from './gameobject';
import { Vector, vec } from '../util/vector';

/**
 * Abstract class for things with bounding boxes. Useful for collision testing.
 */
export default abstract class BoundingBox extends GameObject {
  protected dim: Vector;

  constructor(x1: number, y1: number, x2: number, y2: number) {
    super(vec(x1, y1));
    this.dim = vec(x2 - y1, y2 - y1);
  }

  liesAbove(y: number): boolean {
    return this.getCoords(performance.now() - this.creation).y + this.dim.y < y;
  }

  liesBelow(y: number): boolean {
    return this.getCoords(performance.now() - this.creation).y > y;
  }

  liesLeftOf(x: number): boolean {
    return this.getCoords(performance.now() - this.creation).x + this.dim.x < x;
  }

  liesRightOf(x: number): boolean {
    return this.getCoords(performance.now() - this.creation).x > x;
  }
}
