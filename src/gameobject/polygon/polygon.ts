import { Vector, vec } from "../../util/vector";
import BoundingBox from "../bounding-box";
import PolygonSettings from "./polygon-settings";

const DEFAULT_POLYGON_SETTINGS: PolygonSettings = {
  border: '',
  borderWidth: 1,
  lineCap: 'butt', // hehe
  lineJoin: 'miter',
  fill: 'white',
  miterLimit: 10
};

/**
 * A closed figure made out of lines joining points
 */
export default class Polygon extends BoundingBox {
  /**
   * Points present in this line path
   */
  public points: Vector[];

  /**
   * Settings for the polygon while drawing
   */
  public options: PolygonSettings;

  /**
   * Creates a new `Polygon` with the given points
   * @param points 
   */
  constructor(points: Vector[], options: PolygonSettings = DEFAULT_POLYGON_SETTINGS) {
    let [minX, minY] = points[0].destruct();
    let [maxX, maxY] = points[0].destruct();

    // find bounding box
    for (const point of points.slice(1)) {
      let [x, y] = point.destruct();

      if (x > maxX) {
        maxX = x;
      } else if (x < minX) {
        minX = x;
      }

      if (y > maxY) {
        maxY = y;
      } else if (y < minY) {
        minY = y;
      }
    }

    let topLeft = vec(minX, minY);

    super(minX, minY, maxX, maxY);
    this.points = points.map(point => point.subtract(topLeft));
    this.options = { ...DEFAULT_POLYGON_SETTINGS, ...options };
  }

  draw(ctx: CanvasRenderingContext2D, time: number) {
    let coords = this.getCoords(time);

    ctx.beginPath();
    ctx.moveTo(...coords.destruct());

    for (const point of this.points) {
      ctx.lineTo(...Vector.add(point, coords).destruct());
    }

    ctx.closePath();

    if (this.options.border) {
      ctx.lineWidth = this.options.borderWidth;
      ctx.strokeStyle = this.options.border;
      ctx.lineJoin = this.options.lineJoin;
      ctx.lineCap = this.options.lineCap;
      ctx.stroke();
    }

    if (this.options.fill) {
      ctx.fillStyle = this.options.fill;
      ctx.fill();
    }

    this.drawEvent.fire(this, ctx);
  }
}
