import Polygon from "./polygon";
import { vec } from "../../util/vector";

/**
 * Shorthand to create a rectangle
 * @param x 
 * @param y 
 * @param w 
 * @param h 
 * @param colour 
 */
export function rect(x: number, y: number, w: number, h: number, colour: string = 'white'): Polygon {
  return new Polygon(
    [
      vec(x, y),
      vec(x + w, y),
      vec(x + w, y + h),
      vec(x, y + h)
    ],
    {
      fill: colour,
    }
  );
}

/**
 * Shorthand to create a triangle
 * @param x1 
 * @param y1 
 * @param x2 
 * @param y2 
 * @param x3 
 * @param y3 
 * @param colour 
 */
export function triangle(x1: number, y1: number, x2: number, y2: number, x3: number, y3: number, colour: string = 'white'): Polygon {
  return new Polygon(
    [
      vec(x1, y1),
      vec(x2, y2),
      vec(x3, y3)
    ],
    {
      fill: colour,
    }
  );
}
