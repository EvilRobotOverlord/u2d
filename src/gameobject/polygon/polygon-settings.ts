/**
 * Settings for a `Polygon` while drawing it
 */
export default interface PolygonSettings {
  /**
   * Border color. Leave empty if no border.
   */
  border?: string;

  /**
   * Width of border (in pixels)
   */
  borderWidth?: number;

  /**
   * Fill color. Leave empty if no fill.
   */
  fill?: string;

  /**
   * Tip of endings lines ([`CanvasRenderingContext2D.lineCap`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/lineCap))
   */
  lineCap?: CanvasLineCap;

  /**
   * Corners of meeting points of lines ([`CanvasRenderingContext2D.lineJoin`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/lineJoin))
   */
  lineJoin?: CanvasLineJoin;

  /**
   * Limit if `lineJoin` is set to `'miter'` ([`CanvasRenderingContext2D.miterLimit`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/miterLimit))
   */
  miterLimit?: number;
}
