import SpriteSettings from './sprite-settings';
import Box from '../bounding-box';
import { Vector } from '../../util/vector';

/**
 * Spritesheet-based `GameObject` that auto-updates with each frame
 * 
 * @summary Image drawn onto the canvas
 */
export default class Sprite extends Box {
  private currFrame: number = 0;
  private image: HTMLImageElement;
  private spriteSettings: SpriteSettings;
  private cache: Vector[];
  private prevUpdateFrame: number;

  /**
   * Amount of time (in ms) after which the frame should be updated. This is used in the {@link Sprite#draw|draw function}.
   * 
   * @summary Time to wait before incrementing frames
   */
  public delay: number;

  /**
   * Creates a new `Sprite` and returns it
   * @param x Starting X for the sprite
   * @param y Starting Y for the sprite
   * @param w Width to resize to
   * @param h Height to resize to (-1 for auto scaling)
   * @param resizeDimensions Dimensions to resize to while drawing. Leave as `null` if you do not want it to be resized.
   * @param sprite HTML5 `Image` for the sprite or spritesheet. You can create the `Image` using the `new Image(path)` constructor. Make sure that you pass it only after it has loaded (`img.onload`).
   * @param spriteSettings Settings for `sprite` if it is a spritesheet. Leave blank if it is just an image which you want to draw onto the canvas.
   */
  constructor(x: number, y: number, w: number, h: number, sprite: HTMLImageElement, spriteSettings?: SpriteSettings) {
    super(
      x,
      y,
      spriteSettings.dimensions.x || w,
      spriteSettings.dimensions.y || (h === -1) ? sprite.height * w / sprite.width : h
    );

    this.spriteSettings = spriteSettings || {
      dimensions: this.dim,
      fps: 0,
      padding: new Vector(0, 0),
      startFrame: 0,
      endFrame: 0,
      startAt: new Vector(0, 0),
      frameDimensions: new Vector(sprite.width, sprite.height)
    };

    this.image = sprite;
    this.delay = 1000 / this.spriteSettings.fps;

    this.genCache();
    this.prevUpdateFrame = performance.now();
  }

  private genCache() {
    const cols = Math.floor(
      this.image.width / this.spriteSettings.frameDimensions.y
    );

    const s = this.spriteSettings;

    let row = -1;
    let col = 0;

    for (let frame = s.startFrame; frame <= s.endFrame; frame++) {
      if (!(col = ++col % cols)) {
        row++;
      }

      this.cache.push(new Vector(
        s.startAt.x + s.padding.x + row * s.frameDimensions.x,
        s.startAt.y + s.padding.y + col * s.frameDimensions.y,
      ));
    }
  }

  draw(ctx: CanvasRenderingContext2D, time: number) {
    if (time - this.prevUpdateFrame >= this.delay) {
      this.currFrame = (this.currFrame + 1) % this.cache.length;
      this.prevUpdateFrame += this.delay;
    }

    ctx.drawImage(
      this.image,
      this.cache[this.currFrame].x,
      this.cache[this.currFrame].y,
      this.dim.x,
      this.dim.y
    );
  }
}
