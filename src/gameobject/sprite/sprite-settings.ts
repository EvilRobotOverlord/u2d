import { Vector } from "../../util/vector";

/**
 * Template for object to be passed as arguments to the `constructor` of `Sprite`
 */
export default interface SpriteSettings {
  /**
   * Heigth and width of each sprite
   */
  dimensions: Vector,

  /**
   * Delay (in `ms`) between switching between frames
   */
  fps: number,

  /**
   * Padding between frames (X and Y)
   */
  padding: Vector,

  /**
   * Frame to start at
   */
  startFrame: number,

  /**
   * Frame to end at
   */
  endFrame: number,

  /**
   * Co-ordinates in spritesheet to start drawing at, overrides `startFrame`
   */
  startAt: Vector,

  /**
   * Dimensions of each frame
   */
  frameDimensions: Vector,
}

