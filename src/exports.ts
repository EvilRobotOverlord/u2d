import { Vector, vec } from './util/vector';
import GameObject from './gameobject/gameobject';
import Sprite from './gameobject/sprite/sprite';
import BoundingBox from './gameobject/bounding-box';
import Universe from './universe';
import { triangle, rect } from './gameobject/polygon/quick-polygons';
import Polygon from './gameobject/polygon/polygon';

export {
  Vector,
  vec,
  GameObject,
  Polygon,
  rect,
  triangle,
  Sprite,
  BoundingBox,
  Universe
}
