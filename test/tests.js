/// <reference path="../build/u2d.d.ts"/>

const { Universe, rect, vec, triangle } = U2D;

const u = new Universe(800, 450);
const r = rect(0, 100, 100, 100, 'green');
const t = triangle(100, 100, 100, 200, 200, 200);
const s = rect(100, 100, 50, 50, '#f4d75d');

s.velocity(vec(10, 10));
s.hitEdgeEvent.addListener((_, axis) => {
  let v = s.velocity();

  if (axis === 'x') {
    s.velocity(vec(-v.x, v.y));
  } else {
    s.velocity(vec(v.x, -v.y));
  }
});

r.getCoords = (time) => vec(100 + 100 * Math.sin(time / 250), 100 + 100 * Math.cos(time / 250));

t.canEscapeCanvas(false);

function getRandomVelocity() {
  return vec(5 - Math.random() * 10, 5 - Math.random() * 10);
}

let v = getRandomVelocity();

t.getVelocity = () => {
  if (Math.floor(Math.random() * 50) === 0) {
    v = getRandomVelocity();
  }

  return v;
};

u.add(r);
u.add(t);
u.add(s);
